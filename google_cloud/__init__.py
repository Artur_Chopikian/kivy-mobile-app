import os

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f'/{os.path.abspath(__file__)}/GOOGLE_APPLICATION_CREDENTIALS.json'
