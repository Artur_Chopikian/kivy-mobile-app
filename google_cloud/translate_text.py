from google.cloud import translate
from google.api_core.exceptions import ServiceUnavailable

import os

os.environ[
    'GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/ArturChopikian/PycharmProjects/kivy/GOOGLE_APPLICATION_CREDENTIALS.json'
project_id = os.environ.get("project_id", "healthy-rarity-309112")

assert project_id

parent = f"projects/{project_id}"

translate_client = translate.TranslationServiceClient()


def get_dict_supported_languages(display_language_code: str = 'en') -> dict:
    """
    Get all languages.

    :param display_language_code:
    :return: dict where key is full name and value is abbreviated name of county
    """
    response = translate_client.get_supported_languages(parent=parent, display_language_code=display_language_code)
    languages = response.languages

    return {language.display_name: language.language_code for language in languages}


def detect_language(text):
    """Detecting the language of a text string."""

    # Detail on supported types can be found here:
    # https://cloud.google.com/translate/docs/supported-formats
    response = translate_client.detect_language(
        content=text,
        parent=parent,
        mime_type="text/plain",  # mime types: text/plain, text/html
    )

    # Display list of detected languages sorted by detection confidence.
    # The most probable language is first.
    for language in response.languages:
        # The language detected
        print("Language code: {}".format(language.language_code))
        # Confidence of detection result for this language
        print("Confidence: {}".format(language.confidence))


def translate_text(text: str,
                   source_language_code: str,
                   target_language_code: str) -> str or bool:
    """Translating Text."""

    if target_language_code != source_language_code:

        try:
            # Detail on supported types can be found here:
            # https://cloud.google.com/translate/docs/supported-formats
            response = translate_client.translate_text(
                contents=[text],
                target_language_code=target_language_code,
                source_language_code=source_language_code,
                parent=parent,
            )

        except ServiceUnavailable as e:
            print(e)
            return False

        return response.translations[0].translated_text

    return text


if __name__ == '__main__':
    print(get_dict_supported_languages())
