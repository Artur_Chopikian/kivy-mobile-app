from translate_text import translate_text

with open('correct_list.txt', 'r') as file:
    words = [line.replace('\n', '') for line in file.readlines()]

with open('translated_words.txt', 'a') as file:
    for i, word in enumerate(words[3000:4000]):
        print(i)
        try:
            translated_word = translate_text(text=word, source_language_code='en', target_language_code='uk')
        except:
            pass
        if isinstance(translated_word, str):
            file.write(f'{word} - {translated_word}\n')
        else:
            file.write(f'{word} - ?\n')

# with open('translated_words.txt', 'w') as file:
#     for i, word in enumerate(lines):
#         file.write(f'{lines[i]} - {translated_text[i]}\n')
# print(len(translated_text))
# print(translated_text)
# print(translated_text)
