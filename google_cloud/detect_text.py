from google.cloud import vision
import io
import os
import sys

# os.system('export GOOGLE_APPLICATION_CREDENTIALS="GOOGLE_APPLICATION_CREDENTIALS.json"')
os.environ[
    'GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/ArturChopikian/PycharmProjects/kivy/GOOGLE_APPLICATION_CREDENTIALS.json'

vision_client = vision.ImageAnnotatorClient()


def detect_text(file: str) -> str:
    """Detects text in the file."""

    with io.open(file, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)

    response = vision_client.text_detection(image=image)
    texts = response.text_annotations

    # for text in texts[:1]:
    #     print('\n"{}"'.format(text.description))
    #
    #     vertices = (['({},{})'.format(vertex.x, vertex.y)
    #                  for vertex in text.bounding_poly.vertices])
    #
    #     print('bounds: {}'.format(','.join(vertices)))

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    if texts:
        return texts[0].description.replace('\n', ' ')

    return ''


if __name__ == "__main__":
    def text_from_png_to_txt(path):
        import os
        with open('text.txt', 'w') as txt_file:
            list_folders = os.listdir(path)
            for folder in list_folders:
                if folder != '.DS_Store':
                    for filename in os.listdir(f'{path}/{folder}'):
                        if filename.split('.')[-1] == 'png':
                            detected_text = detect_text(f'{path}/{folder}/{filename}')
                            txt_file.write(detected_text)
                print('good read folder')


    # text_from_png_to_txt('/Users/ArturChopikian/Downloads/all_exams')

    from itertools import groupby
    import string
    import re

    # with open('text_from_pdf.txt', 'w') as file:
    #     for i in range(1, 13):
    #         if i < 10:
    #             i = '0' + str(i)
    #         file.write(
    #             detect_text(
    #                 f'/Users/ArturChopikian/Downloads/EVI_2019-Angl_mova-1_zmina-Zoshyt_1/EVI_2019-Angl_mova-1_zmina-Zoshyt_1-{i}.png'))

    with open('text.txt', 'r') as file:
        uk_letters = 'q w e r t y u i o p a s d f g h j k l z x c v b n m'.split()
        print(uk_letters)
        words = [word.lower() for line in file.readlines() for word in line.split() for letter in word if
                 letter in uk_letters]

    for word in words:
        symbols = ['!', '?', ',', '.', ':', ';', '"']
        for symbol in symbols:
            if symbol in word:
                word = word.replace(symbol, '')

    print(words)

    words_with_duplicates = []

    for word, duplicates in groupby(sorted(words)):  # sort and group duplicates
        count = len(list(duplicates))  # count how many times the word occurs
        words_with_duplicates.append((word, count))

    with open('dublicate_words.txt', 'w') as file:
        for i in sorted(words_with_duplicates, key=lambda item: item[1], reverse=True):
            print(i)
            file.write(f'{i[0]} - {i[1]}\n')
