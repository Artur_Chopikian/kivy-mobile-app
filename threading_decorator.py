import threading


def parallel_execution(func):
    def execute(*args, **kwargs):
        threading.Thread(target=func, args=args, kwargs=kwargs).start()

    return execute
