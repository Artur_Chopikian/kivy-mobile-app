from kivy.uix.screenmanager import Screen

from google_cloud.detect_text import detect_text
from google_cloud.translate_text import translate_text

from languages import LANGUAGES

import time


class CameraWindow(Screen):
    def __init__(self, **kwargs):
        super(CameraWindow, self).__init__(**kwargs)

    def get_photo(self) -> None:
        camera = self.ids['camera']
        timestr = time.strftime("%Y%m%d_%H%M%S")
        file = "./img/IMG_{}.png".format(timestr)
        camera.export_to_png(filename=file)
        text_from_img = detect_text(file=file)

        if text_from_img:
            main_window = self.manager.ids['main_window']

            main_window.ids['text_for_translation'].text = text_from_img

            main_window.ids['translated_text'].text = \
                translate_text(text=text_from_img,
                               target_language_code=LANGUAGES[main_window.ids['trg'].text],
                               source_language_code=LANGUAGES[main_window.ids['src'].text])

        self.manager.current = 'main_window'
