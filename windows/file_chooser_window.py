from kivy.uix.screenmanager import Screen

from google_cloud.detect_text import detect_text

from threading_decorator import parallel_execution


class FileChooserWindow(Screen):
    """
    It is window for choosing different file and check its format, after this file
    """

    def __init__(self, **kwargs):
        super(FileChooserWindow, self).__init__(**kwargs)

    def selected(self, filename: list) -> None:
        """
        We can choose file which only have valid format, else we will have the alert which to show our problem
        :param filename: list where first item it is name of file
        :return:
        """
        print(self.ids['image'].pos, self.ids['error'].pos)
        if self.check_format(filename[0]):
            self.ids['image'].source = filename[0]
        else:
            self.ids['image'].source = ''
            self.ids['error'].text = 'Format, do not valid.'
            print('Format, do not valid.')

    @parallel_execution
    def load_file(self, filename: list) -> None:
        """
        Function for loading file, if it has valid format
        :param filename: list where first item it is name of file
        :return:
        """
        if self.check_format(filename[0]):
            main_window = self.manager.ids['main_window']
            main_window.ids['text_for_translation'].text = detect_text(file=filename[0])

            self.manager.current = 'main_window'

    @staticmethod
    def check_format(filename: str) -> bool:
        """
        Function verifies the format of a file
        :param filename: name of file
        :return:
        """
        valid_formats = ['jpg', 'jpeg', 'png']
        if filename.split('.')[-1] in valid_formats:
            return True

        return False
