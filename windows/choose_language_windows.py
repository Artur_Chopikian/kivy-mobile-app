from kivy.uix.screenmanager import Screen

from kivymd.uix.list import OneLineListItem, IconRightWidget, IconLeftWidget, OneLineIconListItem

from languages import LANGUAGES


def update_recent_list(list_lang: list, current: str) -> list:
    """
    It is function, for source or target lists have only 5 items, where first item it is current language,
    second it is previous item and and so on. It means we have 5 last languages which we used.
    """

    if len(list_lang) == 0:
        list_lang.insert(0, current)
    else:
        if current == list_lang[0]:
            pass
        else:
            if current in list_lang:
                list_lang.remove(current)
            list_lang.insert(0, current)

    if len(list_lang) > 5:
        del list_lang[-1]

    return list_lang


def choose_lang(obj: Screen, lable: OneLineListItem) -> None:
    """
    Called when we click on item of source or target language`s lists
    """

    # create variable main window
    main_window = obj.manager.ids.main_window

    # obj.id is 'src' or 'tgr'
    # updete text of lable
    main_window.ids[obj.id].text = lable.text

    # when we change target language we need to translate text with selected language
    if obj.id == 'trg':
        # update translated text area
        main_window.realtime_translate(text=main_window.ids.text_for_translation.text)

    # close current window and go to main window
    obj.manager.current = 'main_window'


def create_all_languages(obj: Screen) -> None:
    """
    Called when we open our program
    obj is ChooseSourceLangWindow or ChooseTargetLangWindow
    """
    for key, value in LANGUAGES.items():
        obj.ids[f'all_{obj.id}_languages'].add_widget(
            OneLineListItem(text=f"{key}",
                            on_press=lambda lable: choose_lang(obj, lable)))


def update_recent_languages(obj: Screen, list_lang: list, current: str) -> None:
    """
    Called every time when we open ChooseSourceLangWindow or ChooseTargetLangWindow
    This function update recent languages of obj
    """
    for name in update_recent_list(list_lang, current):
        if name == current:
            line = OneLineIconListItem(text=f"{name}", on_press=lambda lable: choose_lang(obj, lable))
            line.add_widget(IconLeftWidget(icon="plus"))
            obj.ids[f'recent_{obj.id}_languages'].add_widget(line)
        else:
            obj.ids[f'recent_{obj.id}_languages'].add_widget(
                OneLineIconListItem(text=f"{name}",
                                    on_press=lambda lable: choose_lang(obj, lable)))


class ChooseSourceLangWindow(Screen):
    __recent_src_lang = []
    id = 'src'

    def set_lists(self, current: str):
        create_all_languages(self)
        update_recent_languages(self, self.__recent_src_lang, current)

    def update(self, current: str):
        # we clear recent languages widget to update its
        self.ids.recent_src_languages.clear_widgets()

        # if we change target language, we update target recent languages
        update_recent_languages(self, self.__recent_src_lang, current)

        # we do to list languages window
        self.manager.current = 'choose_src_lang_window'


class ChooseTargetLangWindow(Screen):
    __recent_trg_lang = []
    id = 'trg'

    def set_lists(self, current: str):
        create_all_languages(self)
        update_recent_languages(self, self.__recent_trg_lang, current)

    def update(self, current: str):
        # we clear recent languages widget to update its
        self.ids.recent_trg_languages.clear_widgets()

        # if we change target language, we update target recent languages
        update_recent_languages(self, self.__recent_trg_lang, current)

        # we do to list languages window
        self.manager.current = 'choose_trg_lang_window'


'''
class ChooseLangWindow(Screen):
    __create_src_list = True
    __create_trg_list = True
    __recent_src_lang = []
    __recent_trg_lang = []

    def build(self, current: str, src=None, trg=None):
        """
        The function is called when we want to change our source or target language.
        Create list of source and target languages if we do not have them,
        and update resent source and target list (languages which we have used recently)

        :param current: it is language's name when we
        :param src: source language - the language from which we will be translated
        :param trg: target language - the language to be translated
        :return:
        """

        # chech if we created source list, if yes, we will not do it again
        if src is not None and self.__create_src_list:
            self.__create_src_list = False
            self.create_all_languages(name='src')

        # chech if we created source list, if yes, we will not do it again
        if trg is not None and self.__create_trg_list:
            self.__create_trg_list = False
            self.create_all_languages(name='trg')

        # we clear recent languages widget to update its
        self.ids.recent_languages.clear_widgets()

        # self.manager.ids[f'choose_{str(src) if src else str(trg)}_lang_window']\
        #     .ids[f'recent_{str(src) if src else str(trg)}_languages'].clear_widgets()

        # if we change source language, we update source recent languages
        if src:
            self.manager.ids.choose_src_lang_window.ids.recent_src_languages.clear_widgets()
            self.update_recent_languages(self.__recent_src_lang, current, name='src')

        # if we change target language, we update target recent languages
        if trg:
            trg_window = self.manager.ids.choose_trg_lang_window
            trg_window.ids.recent_trg_languages.clear_widgets()
            self.update_recent_languages(self.__recent_trg_lang, current, name='trg')

        # we do to list languages window
        self.manager.current = 'list_lang_window'

        # print(self.__recent_src_lang)
        # print(self.__recent_trg_lang)
        print()

    def create_all_languages(self, name: str) -> None:
        """
        Create list of all lenguages
        :param name: it is trg or src (target or source) langeuges
        :return:
        """
        for key, value in LANGUAGES.items():
            self.ids.all_languages.add_widget(
                OneLineListItem(text=f"{key}",
                                on_press=lambda lable: self.choose_lang(lable, name)))

    def update_recent_languages(self, list_lang: list, current: str, name: str):
        for i in self.update_recent_list(list_lang, current):
            self.ids.recent_languages.add_widget(
                OneLineListItem(text=f"{i}",
                                on_press=lambda lable: self.choose_lang(lable, name)))

    def choose_lang(self, lable, name: str):
        print(lable.text, name)
        self.manager.ids.main_window.ids[name].text = lable.text
        self.manager.current = 'main_window'

    @staticmethod
    def update_recent_list(list_lang, current):

        if len(list_lang) == 0:
            list_lang.insert(0, current)
        else:
            if current == list_lang[0]:
                pass
            else:
                if current in list_lang:
                    list_lang.remove(current)
                list_lang.insert(0, current)

        if len(list_lang) > 5:
            del list_lang[-1]

        return list_lang
'''
