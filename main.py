from kivy.factory import Factory
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.scrollview import ScrollView
from kivy.uix.popup import Popup
from kivy.core.audio import SoundLoader

from kivymd.app import MDApp
from kivymd.utils import asynckivy
from kivymd.uix.textfield import MDTextField

# for converting files from mp3 to wav
from pydub import AudioSegment

from time import sleep

import string
import os

# import all windows of app
from windows.camera_window import CameraWindow
from windows.choose_language_windows import ChooseSourceLangWindow, ChooseTargetLangWindow
from windows.file_chooser_window import FileChooserWindow

from languages import LANGUAGES

from google_cloud.translate_text import translate_text
from google_cloud.text_to_speech import text_to_speech

from threading_decorator import parallel_execution


# LANGUAGES = gl.get_dist_supported_languages()


class MyMDTextOutput(MDTextField):
    pass


class ShowErrorWindow(Popup):

    def __init__(self, title, massege, **kwargs):
        super(ShowErrorWindow, self).__init__(**kwargs)

        self.title = title
        self.ids.error_massege.text = massege

        self.open()

    def close(self):
        self.dismiss()


class MyMDTextInput(MDTextField):
    # def _key_down(self, key, repeat=False):
    #     displayed_str, internal_str, internal_action, scale = key
    #
    #     # handle deletion
    #     if (self._selection and
    #             internal_action in (None, 'del', 'backspace', 'enter')):
    #         if internal_action != 'enter' or self.multiline:
    #             self.delete_selection()
    #     elif internal_action == 'del':
    #         # Move cursor one char to the right. If that was successful,
    #         # do a backspace (effectively deleting char right of cursor)
    #         cursor = self.cursor
    #         self.do_cursor_movement('cursor_right')
    #         if cursor != self.cursor:
    #             self.do_backspace(mode='del')
    #     elif internal_action == 'backspace':
    #         self.do_backspace()
    #
    #     # handle action keys and text insertion
    #     if internal_action is None:
    #         self.insert_text(displayed_str)
    #     elif internal_action in ('shift', 'shift_L', 'shift_R'):
    #         if not self._selection:
    #             self._selection_from = self._selection_to = self.cursor_index()
    #             self._selection = True
    #         self._selection_finished = False
    #     elif internal_action == 'ctrl_L':
    #         self._ctrl_l = True
    #     elif internal_action == 'ctrl_R':
    #         self._ctrl_r = True
    #     elif internal_action == 'alt_L':
    #         self._alt_l = True
    #     elif internal_action == 'alt_R':
    #         self._alt_r = True
    #     elif internal_action.startswith('cursor_'):
    #         cc, cr = self.cursor
    #         self.do_cursor_movement(internal_action,
    #                                 self._ctrl_l or self._ctrl_r,
    #                                 self._alt_l or self._alt_r)
    #         if self._selection and not self._selection_finished:
    #             self._selection_to = self.cursor_index()
    #             self._update_selection()
    #         else:
    #             self.cancel_selection()
    #     elif internal_action == 'enter':
    #         self.dispatch('on_text_validate')
    #         if self.text_validate_unfocus:
    #             self.focus = False
    #     elif internal_action == 'escape':
    #         self.focus = False

    def insert_text(self, substring, from_undo=False):

        if len(self.text) == 0:
            substring = f'{substring.upper()}'
        if substring == '\n':
            substring = ''

        return super(MyMDTextInput, self).insert_text(substring, from_undo=from_undo)


class Scroll(ScrollView):
    pass


class MainWindow(Screen):
    # language_list = [key for key in LANGUAGES]
    sound = None

    limit_speaking = 1

    def __init__(self, **kwargs):
        super(MainWindow, self).__init__(**kwargs)

    @parallel_execution
    def realtime_translate(self, text: str) -> None:
        """
        Translate text, use Google Gloud Translate Api
        :param text: text from user input for translation
        """
        print(f'-{text}-', len(text))

        if len(text) > 0 and self.text_is_not_entry(text):

            last_letter = text[-1]

            if last_letter in string.punctuation:
                self.translated_text.text += last_letter

            elif self.src.text != self.trg.text:
                print('called translate_text')
                translated_text = translate_text(text=text,
                                                 target_language_code=LANGUAGES[self.ids['trg'].text],
                                                 source_language_code=LANGUAGES[self.ids['src'].text])
                if isinstance(translated_text, str):
                    self.ids['translated_text'].text = translated_text
                else:
                    ShowErrorWindow(title='Помилка', massege='Перевірте підвлючення до інтернету.')

            # self.translated_text.text = self.text_for_translation.text
            else:
                self.translated_text.text = self.text_for_translation.text

        self.text_for_translation.focus = True

    def press_clean_textarea(self) -> None:
        """
        Clean textareas and removes focus
        """
        self.text_for_translation.text, self.translated_text.text = '', ''
        self.text_for_translation.focus = True
        self.text_for_translation.focus, self.translated_text.focus = False, False

    def press_swipe_lang(self) -> None:
        """
        Swap sourse and turget languages
        """

        self.ids['trg'].text, self.ids['src'].text = self.ids['src'].text, self.ids['trg'].text

    def press_reverse_translate(self) -> None:
        """
        Change text for translation with translated text
        """

        self.text_for_translation.text, self.translated_text.text = \
            self.translated_text.text, self.text_for_translation.text

        self.press_swipe_lang()

        self.text_for_translation.focus, self.translated_text.focus = True, True

    @parallel_execution
    def speaking(self, text, language_key, button) -> None:

        if self.text_is_not_entry(text):

            if self.limit_speaking == 1 and button.text == 'Speak':
                self.limit_speaking += 1
                # use google fucnction
                file = text_to_speech(text, LANGUAGES[language_key])
                # file = 'audio/result.wav'
                convert_file = self.convert_mp3_to_wav(file)

                self.sound = SoundLoader.load(convert_file)
                if self.sound:
                    self.sound.play()

                button.text = 'Stop'

                # sleep(self.sound.length - .1)
                time = self.sound.length - .1
                sleep(time)
                # Clock.schedule_once(lambda br: self.change_stop_to_speak(), time)

                button.text = 'Speak'
                # self.sound = None
                self.limit_speaking -= 1

            elif button.text == 'Stop':
                print('stop')
                button.text = 'Speak'
                self.sound.stop()
                # self.sound = None

    def change_stop_to_speak(self):
        if self.ids.trg_speaking.text == 'Stop':
            self.ids.trg_speaking.text = 'Speak'

    @staticmethod
    def text_is_not_entry(text: str) -> bool:
        if len(text.replace(' ', '')) == 0:
            return False

        return True

    @staticmethod
    def convert_mp3_to_wav(file: str) -> str:
        """
        Convert input mp3 file to mav with AudioSegment from pydub framework
        :param file:
        :return: absolute path to converted file
        """
        input_file = file
        output_file = "audio/result.wav"

        # convert mp3 file to wav file
        sound = AudioSegment.from_mp3(input_file)
        sound.export(output_file, format="wav")

        return f'{os.path.abspath(output_file)}'


class WindowManager(ScreenManager):
    pass


class MainApp(MDApp):
    title = 'Translater'
    manager = None

    def build(self):
        self.manager = Factory.WindowManager()
        # self.manager.size = (480, 680)

        self.set_lists()

        return self.manager

    def set_lists(self):
        async def set_lists():
            self.manager.ids.choose_src_lang_window.set_lists(current=self.manager.ids.main_window.ids.src.text)
            self.manager.ids.choose_trg_lang_window.set_lists(current=self.manager.ids.main_window.ids.trg.text)

        asynckivy.start(set_lists())


if __name__ == '__main__':
    MainApp().run()
