import json

with open('json/languages.json', 'rb') as file:
    LANGUAGES = json.load(file)
